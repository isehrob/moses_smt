# -*- coding: utf-8 -*-

from xmlrpc import client

from . import Sentence


class Translator:
    """ This is translation_site wrapper class for moses"""

    def __init__(self, lang, lstyle):
        self.url = "http://localhost:{}/RPC2"
        if lang == 'ru':
            if lstyle == 'common':
                self.url = self.url.format("9091")
            elif lstyle == 'lex':
                self.url = self.url.format("9092")
        else:
            if lstyle == 'common':
                self.url = self.url.format("9093")
            elif lstyle == 'lex':
                self.url = self.url.format("9094")

    def translate(self, sentence):
        proxy = client.ServerProxy(self.url)
        params = {"text": sentence.get_sentence(), "align": "true"}
        result = proxy.translate(params)
        sent = Sentence()
        sent.set_sentence(self.clean_from_alignment(result['text']))
        sent.set_alignment(result['align'])
        return sent

    @staticmethod
    def clean_from_alignment(text):
        import re
        return re.sub('\|\d+-\d+\|', '', text)






