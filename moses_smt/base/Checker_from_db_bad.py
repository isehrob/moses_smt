# -*- coding:utf-8 -*-

import re
import pickle
import os
from collections import Counter

# from Levenshtein import distance
# spell check script by Peter Norvig
import django
django.setup()
from main_app.models import WordCounts

class Checker:

    def __init__(self):
        self.NWORDS = WordCounts.objects
        self.alphabet = 'ёйцукенгшўзғҳхъфқвапролджэячсмитьбю'

    def levenshtein(self, s, t):
            if s == t: return 0
            elif len(s) == 0: return len(t)
            elif len(t) == 0: return len(s)
            v0 = [None] * (len(t) + 1)
            v1 = [None] * (len(t) + 1)
            for i in range(len(v0)): v0[i] = i
            for i in range(len(s)):
                v1[0] = i + 1
                for j in range(len(t)):
                    cost = 0 if s[i] == t[j] else 1
                    v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
                for j in range(len(v0)):
                    v0[j] = v1[j]
            return v1[len(t)]

    def edits1(self, word):
       splits     = [(word[:i], word[i:]) for i in range(len(word) + 1)]
       deletes    = [a + b[1:] for a, b in splits if b]
       transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b)>1]
       replaces   = [a + c + b[1:] for a, b in splits for c in self.alphabet if b]
       inserts    = [a + c + b for a, b in splits for c in self.alphabet]
       return set(deletes + transposes + replaces + inserts)

    def known_edits2(self, word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if self.NWORDS.filter(word_value=e2))

    def known(self, words):
        return set(w for w in words if self.NWORDS.filter(word_value=w))

    def correct(self, word):
        if self.known([word]):
            status = 'known'
            candidates = self.known([word])
        elif self.known_edits2(word):
            status = 'error'
            candidates = self.known_edits2(word)
        elif self.known(self.edits1(word)):
            status = 'error'
            candidates = self.known(self.edits1(word))
        else:
            status = 'not_found'
            candidates = [word]

        cand_dist = {key: self.levenshtein(word, key) for key in candidates}
        bottom = min(cand_dist.items(), key=lambda x: x[1])[1]
        candidates1 = []
        for ccc in cand_dist.items():
                if ccc[1] == bottom: #or ccc[1] == bottom + 1:
                        candidates1.append(ccc)

        candidates = {}
        for cand in candidates1:
            candidates.update({cand[0]: self.NWORDS.filter(word_value__exact=cand[0])})
        candidates = Counter(candidates)
        return [status, candidates.most_common(6)]

    def check(self, sent, cnt=0):
        words_be_corrected = set(sent.get_sentence().split())
        candidates = []
        cnt_own = 0
        for word in words_be_corrected:
            if not re.match(r'[.,\'"/|()?!@$%&;*:]', word):
                corrected = self.correct(word.lower())
                if corrected[0] == 'error':
                    cnt_own += 1
                    new_word = "#"+str(cnt)+str(cnt_own)+"#"+word
                    if corrected[1]:
                        candidate_list = '<ul id="_'+str(cnt)+str(cnt_own)+'" class="candidates-list">'
                        for cand in corrected[1]:
                            candidate_list += '<li>' + cand[0] + '</li>'
                        candidate_list += '</ul>'
                        candidates.append(candidate_list)
                    sent.set_sentence(sent.get_sentence().replace(word, new_word, 1))
                elif corrected[0] == 'not_found':
                    new_word = "$"+word
                    sent.set_sentence(sent.get_sentence().replace(word, new_word, 1))
        sent.set_alignment(candidates)
        return sent

if __name__ == '__main__':
    # args = sys.argv
    # print(args)
    sp = Checker()
    inp2 = 'кетаётиб'
    inp1 = 'biz bozrga boramiz'
    inp = ['бнз бир куни кучада кетаётиб бурини куриб колдик.', 'бнз бир куни кучада кетаётиб бурини куриб колдик.']
    # print('input: ', inp)
    # #otp = sp.correct(inp)
    # #print('output: ', otp)
    # cc = CorpusCorrector()
    # #print("started correcting")
    # print(cc.correct_corpus(inp))
    # #print("it is Done!")
    import time

    # start = time.clock()
    # sp.edits1(inp1)
    # print('Time for edits1')
    # print(str(time.clock() - start))
    #
    # start = time.clock()
    # sp.known_edits2(inp1)
    # print('Time for known edits')
    # print(str(time.clock() - start))

    # start = time.clock()
    # sett = sp.known(inp2)
    # print('Time for known')
    # print(str(time.clock() - start))
    # print(sett)

    # start = time.clock()
    # sp.levenshtein(inp1, inp)
    # print('Time for levenshtein')
    # print(str(time.clock() - start))

    start = time.clock()
    res = sp.check(inp2)
    print('Time for correcting one word')
    print(str(time.clock() - start))
    print(res[0])


