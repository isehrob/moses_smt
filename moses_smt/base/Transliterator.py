# Transliteration module


class Transliterator:

    def __init__(self, orientation='crl2ltn'):
        self.orientation = orientation

    def lat_2_cyr(self, inp, trs):
        translited = []
        for word in inp.split():
            for i, l in enumerate(word):
                try:
                    if i == 0 and l in ['e', 'E']:
                        word = trs.e_disambugate(i, word)
                    elif word[i] in ['e', 'E']:
                        word = trs.e_replace(i, word)
                    elif word[i] and trs.is_sound(i, word):
                        old = word[i]+word[i+1]
                        word = word.replace(old, trs.sounds[old], 1)
                    elif word[i] and trs.is_quote(i, word):
                        word = trs.quotate(i, word)
                    elif word[i] and trs.is_u(i, word):
                        old = word[i]+word[i+1]
                        word = word.replace(old, trs.us[old], 1)
                    elif word[i]:
                        try:
                            word = word.replace(word[i], trs.alphabet[word[i]], 1)
                        except KeyError:
                            # raise
                            continue
                except IndexError:
                    # raise
                    break
            translited.append(word)
        return ' '.join(translited)

    def cyr_2_lat(self, inp, trs):
        translited = []
        for word in inp.split():
            for i, l in enumerate(word):
                try:
                    if i == 0 and l in ['е', 'Е']:
                        word = trs.e_disambugate(word)
                    else:
                        try:
                            word = word.replace(l, trs.alphabet[l], 1)
                        except KeyError:
                            continue
                except IndexError:
                    # raise
                    break
            translited.append(word)
        return ' '.join(translited)

    def trans(self, inp):
        from translation_app.helpers import helper_cls
        if self.orientation == 'ltn2crl':
            trs = helper_cls.Cyrillic()
            return self.lat_2_cyr(inp, trs)
        elif self.orientation == 'crl2ltn':
            trs = helper_cls.Latinic()
            return self.cyr_2_lat(inp, trs)
        else:
            return False

if __name__ == '__main__':
    transliterate = Transliterator('ltn2crl')
    inputs = "O`zbekiston haqida qonun qabul qilindi"
    print(transliterate.trans(inputs))

    transliterate = Transliterator('crl2ltn')
    inputs = "дарё Тошқин сувлар. ТЎЛҚИН 1ўтолмаймано ер ва эр МАЬНОЛИ қаради"
    print(transliterate.trans(inputs))
