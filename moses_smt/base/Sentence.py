
class Sentence:
    """
    This class models the basic sentence structure
    The composite sentences class extends this class
    and it implies the composite pattern
    """

    def __init__(self):
        self.children = None
        self.sentence = None
        self.alignment = None

    def __str__(self):
        if self.sentence:
            return self.get_sentence()
        else:
            return self.children

    def __getitem__(self, index):
        return self.children[index]

    def set_sentence(self, sent):
        self.sentence = sent.strip()

    def get_sentence(self):
        return self.sentence

    def set_alignment(self, align):
        self.alignment = align

    def get_alignment(self):
        return self.alignment

    def append_child(self, child):
        if self.children is None:
            self.children = []
        self.children.append(child)

    def accept(self, visitor):
        visitor.visit_this(self)
