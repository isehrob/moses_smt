# -*- coding:utf-8 -*-

from translation_app.visitors import BasicVisitor
from translation_app.base import Checker
from translation_app.helpers import helper_fns, helper_cls


class SpellingApp:

    def __init__(self, kwargs):

        self.visitors = [
            BasicVisitor(helper_fns.tokenize),
        ]

        self.out_visitors = [
            BasicVisitor(helper_fns.detokenize),
        ]
        # if kwargs['inputLangChar'][0] == 'ltn':
        #     self.visitors.insert(0, BasicVisitor(helper_cls.Lat2Cyr))
        #     self.out_visitors.insert(0, BasicVisitor(helper_cls.Cyr2Lat))

        self.checker = Checker()
        self.splittor = helper_fns.split
        self.finder = helper_fns.find_input_type

    def do_spelling(self, input_text):
        return self.checker.check(input_text)

    # def do_spelling(self, input_text):
    #     sentences = self.splittor(self.finder(input_text), input_text)
    #
    #     for sent in sentences:
    #         for visitor in self.visitors:
    #             sent.accept(visitor)
    #
    #     from translation_app.base import Sentence
    #     spelleds = Sentence()
    #
    #     # translating sentences with moses
    #     # TODO! this not such a good testing method!
    #     try:
    #         for i, sent in enumerate(sentences):
    #             spelled = self.checker.check(sent, i)
    #             spelleds.append_child(spelled)
    #     except ConnectionRefusedError:
    #         return False
    #
    #     # applying visitors to translation sentences
    #
    #     for spelled in spelleds:
    #         for visitor in self.out_visitors:
    #             spelled.accept(visitor)
    #
    #     return spelleds


if __name__ == '__main__':
    # args = sys.argv
    # print(args)
    sp = SpellChecker('ltn2crl')
    inp2 = 'кетаётиб'
    inp1 = 'biz bozrga boramiz'
    inp = ['бнз бир куни кучада кетаётиб бурини куриб колдик.', 'бнз бир куни кучада кетаётиб бурини куриб колдик.']
    # print('input: ', inp)
    # #otp = sp.correct(inp)
    # #print('output: ', otp)
    # cc = CorpusCorrector()
    # #print("started correcting")
    # print(cc.correct_corpus(inp))
    # #print("it is Done!")
    import time

    # start = time.clock()
    # sp.edits1(inp1)
    # print('Time for edits1')
    # print(str(time.clock() - start))
    #
    # start = time.clock()
    # sp.known_edits2(inp1)
    # print('Time for known edits')
    # print(str(time.clock() - start))

    # start = time.clock()
    # sett = sp.known(inp2)
    # print('Time for known')
    # print(str(time.clock() - start))
    # print(sett)

    # start = time.clock()
    # sp.levenshtein(inp1, inp)
    # print('Time for levenshtein')
    # print(str(time.clock() - start))

    start = time.clock()
    res = sp.correct_corpus(inp1)
    print('Time for correcting one word')
    print(str(time.clock() - start))
    print(res[0])
    print(res[1])


