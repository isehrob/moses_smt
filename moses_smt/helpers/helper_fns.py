"""
Other helper functions
"""
import re


def detokenize(input_text):
    """
    Helper function for text detokenization after processing
    :param input_text: string type
    :return:
    """
    pattern = r"\s+([?.,!)\"'])"
    if re.search(r"[(]", input_text):
        input_text = re.sub(r"([(])\s+", r"\1", input_text)
    return re.sub(pattern, r"\1", input_text)


def fix_alignment(inp):
    """
    Helper function for alignment creation according to
    alignment information given by moses server
    :param inp:
    :return:
    """
    alignment = []
    for i, align in enumerate(inp):
        if align['tgt-start'] == -1 and align['src-end'] == -1:
            continue
        try:
            end = align['tgt-start'] + ((inp[i+1]['tgt-start']-1) - align['tgt-start'])
        except IndexError:
            end = align['src-end']
        al = str(align['tgt-start']) + '%' + str(end) + '|' + str(align['src-start']) + '%' + str(align['src-end'])
        alignment.append(al)
    return ';'.join(alignment)


def encode_format(inp):
    """
    Helper function for preserving input text formatting
    :param inp:
    :return:
    """
    # TODO: need to improve the function
    inp = re.sub(r'\t', '&', inp)
    inp = re.sub(r'\n', '^', inp)
    inp = re.sub(r'\r\n', '^', inp)
    return inp


def decode_format(inp):
    """
    Helper function for restoring the input text format
    :param inp:
    :return:
    """
    # TODO: need to improve the function
    inp = re.sub(r'&', '&nbps;', inp)
    inp = re.sub(r'\^', '<br>', inp)
    return inp


def find_input_type(input_text):
    """
    Helper function for defining the structure of the input text
    whether it is one sentence or collection of sentences
    :param input_text:
    :return:
    """
    pattern = r'((?<=[a-z0-9а-яғҳўқ][.?!])|(?<=[a-z0-9а-яғҳўқ][.?!]\"))' \
              r'(\s|\r\n)((?=\"?[A-ZА-ЯҒҲЎҚ])|(?=[0-9][.)]))'
    if len([x for x in re.split(pattern, input_text) if x.strip() != ""]) > 1:
        return True
    return False


def lowercase_input(input_text):
    """
    Helper function for lowercasing the input text
    :param input_text:
    :return:
    """
    return input_text.lower()


def split(multi, input_text):
    """
    Helper function for splitting the input text into
    sentence objects if it is multi
    :param multi: bool
    :param input_text: string
    :return:
    """
    from translation_app.base.Sentence import Sentence
    sents = Sentence()
    if multi:
        pattern = r'((?<=[a-zа-яғҳўқ][.?!])|(?<=[a-zа-яғҳўқ][.?!]\"))' \
                  r'(\s|\r\n)((?=\"?[A-ZА-ЯҒҲЎҚ])|(?=[0-9][.)]))'
        for x in re.split(pattern, input_text):
            if x.strip() != "":
                sent = Sentence()
                sent.set_sentence(x)
                sents.append_child(sent)
        return sents
    sent = Sentence()
    sent.set_sentence(input_text)
    sents.append_child(sent)
    return sents


def tokenize(text):
    """
    Helper function for tokenizing the input text
    for further processing
    :param text:
    :return:
    """
    pattern = r"([\"()'.?,!])"
    replacement = r" \1 "
    text = re.sub(pattern, replacement, text)
    return re.sub(r"(\d+)\s([.,])\s(\d+)", r"\1\2\3", text)


def truecase_output(output_text):
    """
    Helper function for trucasing the input text after
    processing
    :param output_text:
    :return:
    """
    return output_text.capitalize()


def remove_wtsp(input_text):
    """
    Helper function for removing two or more white spaces
    :param input_text:
    :return:
    """
    return re.sub(r"\s{2,}", " ", input_text)


def wrap_dates(input_text):
    """
    Helper function for wrapping date string in the text
    with appropriate xml tags for moses
    :param input_text:
    :return:
    """
    dates_fixed = []
    for word in input_text.split():
        if re.match(r"^(\d+[.,]*\d+[.,]*)+$", word):
            dates_fixed.append('<ne translation="' + word + '" prob="1">' + word + '</ne>')
        else:
            dates_fixed.append(word)
    return " ".join(dates_fixed)


def insert_walls(input_text):
    """
    Helper function for inserting wall xml tags into text for moses
    :param input_text:
    :return:
    """
    return input_text


def wrap_zones(input_text):
    """
    Helper function for wrapping zones in the text for moses
    :param input_text:
    :return:
    """
    zones_fixed = []
    first = False
    second = False
    for word in input_text.split():
        if word == "\"":
            if not first:
                first = True
                zones_fixed.append("<zone> " + word)
            elif not second:
                second = True
                zones_fixed.append(word + " </zone>")
            else:
                zones_fixed.append(word)
        else:
            zones_fixed.append(word)
    result = " ".join(zones_fixed)

    if first and not second:
        result = result.replace("<zone>", "")
        result = result.replace("\"", "")
    return result