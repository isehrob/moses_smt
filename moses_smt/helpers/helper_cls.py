"""
Helper classes for our apps
"""

from ..base.Transliterator import Transliterator


class Cyrillic:

    alphabet = {
        "a": "а", "b": "б", "d": "д", "e": ["е", "э"], "f": "ф", "g": "г", "h": "ҳ", "i": "и", "j": "ж", "k": "к",
        "l": "л", "m": "м", "n": "н", "o": "о", "p": "п", "q": "қ", "r": "р", "s": "с", "t": "т", "u": "у", "v": "в",
        "x": "х", "y": "й", "z": "з", "A": "А", "B": "Б", "D": "Д", "E": ["Е", "Э"], "F": "Ф",
        "G": "Г", "H": "Ҳ", "I": "И", "J": "Ж", "K": "К", "L": "Л", "M": "М", "N": "Н", "O": "О", "P": "П", "Q": "Қ",
        "R": "Р", "S": "С", "T": "Т", "U": "У", "V": "В", "X": "Х", "Y": "Й", "Z": "З",
    }

    sounds = {
        'yo': 'ё', 'yu': 'ю', 'ya': 'я', 'ye': 'е', 'Yo': 'Ё', 'Yu': 'Ю', 'Ya': 'Я', 'Ye': 'Е', "ch": "ч", "CH": "Ч",
        "sh": "ш", "SH": "Ш", "ng": "нг", "NG": "НГ", 'ts': "ц", 'TS': "Ц"
    }

    misc = {
        "'": [["ь", "ъ"], ["Ь", "Ъ"]]
    }

    us = {
        "G`": "Ғ", "O`": "Ў", "g`": "ғ", "o`": "ў",
    }

    vowels = ['a', 'o', 'i', 'u', "o'", 'e']

    def is_sound(self, i, inp):
        try:
            if inp[i] + inp[i+1] in [x for x in self.sounds.keys()]:
                return True
        except IndexError:
            return False
        return False

    def is_u(self, i, inp):
        try:
            if inp[i] + inp[i+1] in self.us.keys():
                return True
        except IndexError:
            return False
        return False

    def is_quote(self, i, inp):
        if inp[i] == "'" and inp[i-1] not in ['o', 'O', 'g', 'G']:
            return True
        return False

    def quotate(self, i, inp):
        if inp[i-1] in self.vowels:
            if inp.isupper():
                return inp.replace(inp[i], self.misc[inp[i]][1][1])
            else:
                return inp.replace(inp[i], self.misc[inp[i]][0][1])
        else:
            if inp.isupper():
                return inp.replace(inp[i], self.misc[inp[i]][1][0])
            else:
                return inp.replace(inp[i], self.misc[inp[i]][0][0])

    def e_disambugate(self, i, inp):
        return inp.replace(inp[i], self.alphabet[inp[i]][1], 1)

    def e_replace(self, i, inp):
        return inp.replace(inp[i], self.alphabet[inp[i]][0], 1)


class Latinic:

    alphabet = {
        'а': "a", 'б': "b", 'в': "v", 'г': "g", 'д': "d", 'е': "e", 'ё': "yo", 'ж': "j", 'з': "z", 'й': "y", 'и': "i", 'к': "k", 'л': "l",
        'м': "m", 'н': "n", 'о': "o", 'п': "p", 'р': "r", 'с': "s", 'т': "t", 'у': "u", 'ф': "f", 'ц': "ts", 'ч': "ch", 'ш': "sh",
        'ъ': "'", 'ь': "'", 'э': "e", 'ю': "yu", 'я': "ya", 'ў': "o`", 'қ': "q", 'ғ': "g`", 'ҳ': "h",
        'А': "A", 'Б': "B", 'В': "V", 'Г': "G", 'Д': "D", 'Е': "E", 'Ё': "Yo", 'Ж': "J", 'З': "Z", 'Й': "Y", 'И': "I", 'К': "K", 'Л': "L",
        'М': "M", 'Н': "N", 'О': "O", 'П': "P", 'Р': "R", 'С': "S", 'Т': "T", 'У': "U", 'Ф': "F", 'Ц': "TS", 'Ч': "Ch", 'Ш': "Sh",
        'Ъ': "'", 'Ь': "'", 'Э': "E", 'Ю': "Yu", 'Я': "Ya", 'Ў': "O`", 'Қ': "Q", 'Ғ': "G`", 'Ҳ': "H",
    }

    def e_disambugate(self, inp):
        if inp.isupper():
            return inp.replace(inp[0], 'Ye', 1)
        else:
            return inp.replace(inp[0], 'ye', 1)


class Lat2Cyr(Transliterator):

    def __init__(self):
        super(Lat2Cyr, self).__init__()
        self.main = self.trans

    def trans(self, inp):
        trs = Cyrillic()
        return self.lat_2_cyr(inp, trs)


class Cyr2Lat(Transliterator):

    def __init__(self):
        super(Cyr2Lat, self).__init__()
        self.main = self.trans

    def trans(self, inp):
        trs = Latinic()
        return self.cyr_2_lat(inp, trs)