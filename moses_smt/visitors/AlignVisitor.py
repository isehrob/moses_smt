''' Alignment visitor class extends basic visitor class'''

from . import BasicVisitor


class AlignVisitor(BasicVisitor):

    def __init__(self, obj):
        super(AlignVisitor, self).__init__(obj)

    def visit_this(self, sent):
        sent.set_alignment(self.visitor_func(sent.get_alignment()))
