"""
This is abstract visitor class
This class implements Visitor pattern
"""


class BasicVisitor:

    def __init__(self, obj):
        self.visitor_func = obj

    def visit_this(self, sent):
        if type(self.visitor_func) == type:
            sent.set_sentence(self.visitor_func().main(sent.get_sentence()))
        else:
            sent.set_sentence(self.visitor_func(sent.get_sentence()))
