from .visitors import BasicVisitor, AlignVisitor
from .helpers import helper_cls, helper_fns
from .base import Translator


class TranslationApp:
    """
        Main application file
    """

    # lets initialize visitors needed which take some action on sentences

    def __init__(self, kwargs):

        # visitors for processing input sentences
        self.visitors = [
            BasicVisitor(helper_fns.lowercase_input),
            BasicVisitor(helper_fns.tokenize),
            # BasicVisitor(helper_fns.wrap_zones),
            # BasicVisitor(helper_fns.wrap_dates),
            BasicVisitor(helper_fns.insert_walls),
            BasicVisitor(helper_fns.remove_wtsp),
        ]

        # visitors for processing output sentences
        self.translationVisitors = [
            BasicVisitor(helper_fns.detokenize),
            BasicVisitor(helper_fns.truecase_output),
            AlignVisitor(helper_fns.fix_alignment),
        ]

        input_lang = 'ru' if kwargs.get('forward', False) == 'true' else 'uz'
        self.translator = Translator(input_lang, kwargs.get('lstyle'))

    # main method of translation app
    # it composes all parts of the application in order to
    # carry out whole translation process

    # it gets raw text @string as input and outputs a special Sentence @object
    # with its children (also Sentence @objects) who encapsulates real translations and their alignment
    # information with input sentences

    def do_translation(self, input_text):

        # here we find out whether input is a sentence or a collection of sentences
        # and create Sentence objects with them

        sentences = helper_fns.split(helper_fns.find_input_type(input_text), input_text)

        # applying visitors to input sentences

        for sent in sentences:
            for visitor in self.visitors:
                sent.accept(visitor)
        from translation_app.base import Sentence

        translation = Sentence()

        # translating sentences with moses
        # TODO! this is not such a good testing method!
        try:
            for sent in sentences:
                transed = self.translator.translate(sent)
                translation.append_child(transed)
        except ConnectionRefusedError as e:
            # print(e)
            return False

        # applying visitors to translation sentences
        for trans in translation:
            for visitor in self.translationVisitors:
                trans.accept(visitor)

        return translation


if __name__ == "__main__":
    app = TranslationApp(('uz', 'crl'))
    text = "Был принять закон об Узбекистане."
    text1 = "O'zbekiston haqida qonun qabul qilindi"
    text2 = "Ўзбекистон ҳақида қонун қабул қилинди"
    translation = app.do_translation(text)
    if translation:
        for trans in translation:
            print(trans.get_sentence())
    else:
        print('Not working')

