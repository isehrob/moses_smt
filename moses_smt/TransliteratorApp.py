# -*- coding:utf-8 -*-

from translation_app.visitors import BasicVisitor
from translation_app.helpers import helper_fns, helper_cls


class TransliteratorApp:

    # forward: ltn => crl; backward: crl => ltn
    def __init__(self, kwargs):

        self.visitors = []

        # TODO: what are you doing here? looks so ugly
        if 'inputLang' in kwargs:
            if kwargs['inputLang'][0] == 'ltn':
                self.visitors.append(BasicVisitor(helper_cls.Lat2Cyr))
            else:
                self.visitors.append(BasicVisitor(helper_cls.Cyr2Lat))

    def do_translit(self, input_text):
        sentences = helper_fns.split(helper_fns.find_input_type(input_text), input_text)

        for sent in sentences:
            for visitor in self.visitors:
                sent.accept(visitor)
        return sentences


if __name__ == '__main__':
    pass


