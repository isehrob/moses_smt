class MyApp:
    def setChild(self,child):
        self.child = child
    def getChild(self):
        return self.child
    def accept(self, visitor):
        visitor.visitThis(self)

class YourApp:
    def visitThis(self, app):
        app.child += "changed!"

if __name__ == '__main__':
    visitor = YourApp()
    visitee = MyApp()
    visitee.setChild("this is my child")
    visitee.accept(visitor)
    print(visitee.getChild())
